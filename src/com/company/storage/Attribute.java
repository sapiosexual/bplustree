package com.company.storage;

/**
 *
 * @author Admin
 */
public class Attribute {
    
    String name;
    AttributeType type;

    public Attribute(String name, AttributeType type) throws Exception {
        if(name.contains(";") || name.contains(":"))
            throw new Exception("; and ; not allowed");
        this.name = name;
        this.type = type;
    }
    
    
    public enum AttributeType {
        Integer("integer"),
        String("string");
        
        String name;

        private AttributeType(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name; //To change body of generated methods, choose Tools | Templates.
        }
        
        public static AttributeType getType(String name) throws Exception {
            switch(name) {
                case "integer":
                    return Integer;
                case "string":
                    return String;
                default:
                    throw new Exception();
            }
        }
    }
    
}
