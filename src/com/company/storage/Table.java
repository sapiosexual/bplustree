package com.company.storage;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Admin
 */
public class Table {
    
    RandomAccessFile file;
    long firstRow;
    
    final int INTEGER_SIZE = 10;
    
    List<Attribute> attributes = new ArrayList<>();
    Map<Attribute, Index> indexes = new HashMap<>();

    public Table(File file) throws Exception {
        this.file = new RandomAccessFile(file, "rw");
        initTable();
    }
    
    private void initTable() throws Exception {
        file.seek(0);
        int structLength = file.readInt();
        byte[] structBytes = new byte[structLength];
        file.read(structBytes);
        String struct = new String(structBytes);
        String[] unparsedAttributes = struct.split(";");
        for (String unparsedAttribute : unparsedAttributes) {
            String[] attrComponents = unparsedAttribute.split(":");
            attributes.add(new Attribute(attrComponents[0], Attribute.AttributeType.getType(attrComponents[1])));
        }
        firstRow = file.getFilePointer();
    }
    
    public void addIndex(Index i, Attribute a) {
        indexes.put(a, i);
    }
    
    public static void initTable(File file,
                                 List<Attribute> attributes,
                                 int k) throws IOException {
        file.createNewFile();
        RandomAccessFile raf = new RandomAccessFile(file, "rw");
        String structure = "";
        structure = attributes.stream().map((a) -> a.name + ":" + a.type + ";").reduce(structure, String::concat);
        byte[] bytes = structure.getBytes();
        raf.writeInt(bytes.length);//length of structure
        raf.write(bytes);//structure of table
        raf.close();
    }
    
    public List<Row> select(Attribute attr, Object key) {
        Index index = indexes.get(attr);
        List<Row> result = new ArrayList<>();
        if(index != null) {
            List<Long> links = index.select(key);
            try {
                for(Long l : links) {
                    result.add(select(l));
                }
            } catch(Exception e) {
                
            }
        }
        return result;
    }
    
    public void delete(Attribute attr, Object key) {
        Index index = indexes.get(attr);
        if(index != null) {
            List<Long> links = index.select(key);
            List<Row> rows = new ArrayList<>();
            try {
                for(Long link : links) {
                    rows.add(parseRow(link));
                }
                for(Long l : links) {
                    delete(l);
                }
                attributes.stream().forEach((a) -> {
                    Index i = indexes.get(a);
                    if (i != null) {
                        rows.stream().forEach((row) -> {
                            i.delete(row.getValue(a.name));
                        });
                    }
                }); 
            } catch(Exception e) {
                
            }
        }
    }
    
    public void insert(Row row) {
        try {
            long pointer = file.length();
            insert(pointer, row);
            attributes.stream().forEach((attr) -> {
                Index index = indexes.get(attr);
                if (index != null) {
                    index.insert(row.getValue(attr.name), pointer);
                }
            });
        } catch(Exception e) {
            
        }
    }
    
    public void update(Row row, Attribute attr) {
        delete(attr, row.getValue(attr.name));
        insert(row);
    }
    
    private Row select(long pointer) throws IOException {
        return parseRow(pointer);
    }
    
    private void insert(long pointer, Row row) throws IOException {
        file.seek(pointer);
        file.writeLong(file.length());
        writeRow(row, file.length());
    }
    
    private void delete(long pointer) throws IOException {
        eraseRow(pointer);
    }
    
    public void update(Row row) throws IOException {
        delete((Integer)row.getValue("id"));
        insert((Integer)row.getValue("id"), row);
    }
    
    private void writeRow(Row row, long pointer) throws IOException {
        for(Attribute attr : attributes) {
            switch(attr.type) {
                case Integer:
                    file.seek(pointer);
                    writeInt((Integer)row.getValue(attr.name), pointer);
                    pointer += INTEGER_SIZE;
                break;
                
                case String:
                    file.seek(pointer);
                    String string = (String)row.getValue(attr.name);
                    writeString(string, pointer);
                    pointer = pointer + 4 + string.length();
                break;
            }
        }
    }
    
    private void writeString(String string, long pointer) throws IOException {
        file.seek(pointer);
        byte[] bytes = string.getBytes();
        file.writeInt(bytes.length);
        file.write(bytes);
    }
    
    private void writeInt(Integer integer, long pointer) throws IOException {
        //2147483647 - max int
        String intString = "";
        int powerOfTen = 100000000;
        if(integer/powerOfTen >= 10) {
            intString = integer.toString();
        } else {
            intString += "0";
            while(integer/powerOfTen == 0) {
                intString += "0";
                powerOfTen/=10;
            }
            intString += integer;
        }
        file.seek(pointer);
        file.write(intString.getBytes());
    }
    
    private int parseInt(long pointer) throws IOException {
        byte[] array = new byte[INTEGER_SIZE];
        file.seek(pointer);
        file.read(array);
        return Integer.parseInt(new String(array));
    }
    
    private String parseString(long pointer, int size) throws IOException {
        byte[] array = new byte[size];
        file.seek(pointer);
        file.read(array);
        return new String(array);
    }
    
    private void eraseRow(long pointer) throws IOException {
        byte[] bytes;
        for(Attribute attr : attributes) {
            switch(attr.type) {
                case Integer:
                    file.seek(pointer);
                    bytes = new byte[INTEGER_SIZE];
                    file.write(bytes);
                    pointer += INTEGER_SIZE;
                break;
                
                case String:
                    file.seek(pointer);
                    int size = file.readInt();
                    file.seek(pointer);
                    file.writeInt(0);
                    bytes = new byte[size];
                    file.write(bytes);
                    pointer += size + 4;
                break;
            }
        }
    }
    
    private Row parseRow(long pointer) throws IOException {
        Row result = new Row();
        for(Attribute attr : attributes) {
            switch(attr.type) {
                case Integer:
                    result.addValue(attr.name, parseInt(pointer));
                    pointer += INTEGER_SIZE;
                break;
                
                case String:
                    file.seek(pointer);
                    int size = file.readInt();
                    result.addValue(attr.name, parseString(pointer + 4, size));
                    pointer += size + 4;
                break;
            }
        }
        return result;
    }
    
    private static byte[] longToBytes(long x) {
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.putLong(x);
        return buffer.array();
    }
    
    private static byte[] intToBytes(int x) {
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.putInt(x);
        return buffer.array();
    }
    
    private static int powerLikeAGod(int x, int n) {
        if(n < 0) return -1;
        else if(n == 0) return 1;
        else if(n == 1) return x;
        else if(n%2 == 0) return powerLikeAGod(x * x,  n / 2);
        else return x * powerLikeAGod(x * x, (n - 1) / 2);
    }
    
}
