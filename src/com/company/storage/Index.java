package com.company.storage;

import com.company.bplustree.BPlusTree;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author Admin
 */
public class Index {
    
    private BPlusTree tree;
    private Attribute attribute;

    public Index(File file, Attribute attribute) throws IOException {
        this.tree = new BPlusTree(file);
        this.attribute = attribute;
    }
    
    public void insert(Object key, long pointer) {
        tree.insert(key.hashCode(), pointer);
    }
    
    public List<Long> select(Object key) {
        return tree.select(key.hashCode());
    }
    
    public void delete(Object key) {
        tree.delete(key.hashCode());
    }
}
