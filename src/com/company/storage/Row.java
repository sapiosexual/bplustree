package com.company.storage;

import java.util.HashMap;

/**
 *
 * @author Admin
 */
public class Row {
    
    HashMap<String, Object> values = new HashMap<>();
    
    public void addValue(String name, Object value) {
        values.put(name, value);
    }
    
    public Object getValue(String name) {
        return values.get(name);
    }
    
}
