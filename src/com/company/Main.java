package com.company;

import com.company.bplustree.BPlusTree;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

public class Main {
    
    static ArrayList<Integer> keyValues = new ArrayList<>();
    
    static String[] hexDigits = {
                                 "0", "1", "2", "3", 
                                 "4", "5", "6", "7", 
                                 "8", "9", "A", "B", 
                                 "C", "D", "E", "F"
                                };

    public static void main(String[] args) throws IOException {
//        testSearch();
        testFile();
    }
    
    public static void testStorage() {
        File studentsFile = new File("students.txt");
        //File students
        if(!studentsFile.exists()) {
            ArrayList<Attribute> attributes = new ArrayList<>();
            attributes.add(new Attribute("id", Attribute.AttributeType.Integer));
            attributes.add(new Attribute("name", Attribute.AttributeType.String));
            attributes.add(new Attribute("email", Attribute.AttributeType.String));
            attributes.add(new Attribute("address", Attribute.AttributeType.String));
            Table.initTable(studentsFile, attributes, 2);
        }
        Table students = new Table(studentsFile);
        
        Row row = new Row();
        row.addValue("id", 1);
        row.addValue("name", "dima");
        row.addValue("email", "mylo");
        row.addValue("address", "dom");
        
        students.insert(1, row);
        
        Row row1 = students.select(1);
        
        System.out.println(row1.getValue("id"));
        System.out.println(row1.getValue("name"));
        System.out.println(row1.getValue("email"));
        System.out.println(row1.getValue("address"));
        
        students.delete(1);
        
        row1 = students.select(1);
        if(row1 != null) {
            System.out.println(row1.getValue("id"));
            System.out.println(row1.getValue("name"));
            System.out.println(row1.getValue("email"));
            System.out.println(row1.getValue("address"));
        }
        
        students.insert(1, row);
        
        row = new Row();
        row.addValue("id", 1);
        row.addValue("name", "privet");
        row.addValue("email", "eshe");
        row.addValue("address", "raz");
        
        students.update(row);
        
        row1 = students.select(1);
        if(row1 != null) {
            System.out.println(row1.getValue("id"));
            System.out.println(row1.getValue("name"));
            System.out.println(row1.getValue("email"));
            System.out.println(row1.getValue("address"));
        }
    }
    
    public static void testFile() throws IOException {
        File file = new File("btest.txt");
        if(!file.exists()) {
            file.createNewFile();
            BPlusTree.createNewTree(file, 7);
        }
        
        BPlusTree tree = new BPlusTree(file);
        for(int i = 0; i < 2000; i+=5) {
            tree.insert(i, 7000 - i);
        }
        for(int i = 0; i < 2000; i+=10) {
            tree.delete(i);
        }
        
        FileInputStream fis = new FileInputStream(file);
        byte[] source = new byte[(int)file.length()];
        fis.read(source);
        System.out.println(byteArrayToHexString(source));
        ArrayList<Long> arrayList = tree.selectAllValues();
        for(Long l : arrayList) {
            System.out.print(l + " ");
        }
        System.out.println();
        tree.delete(700);
        arrayList = tree.select(700);
        for(Long l : arrayList) {
            System.out.print(l + " ");
        }
        System.out.println();
        arrayList = tree.select(655);
        for(Long l : arrayList) {
            System.out.print(l + " ");
        }
        System.out.println();
        arrayList = tree.select(600);
        for(Long l : arrayList) {
            System.out.print(l + " ");
        }
        System.out.println();
        arrayList = tree.select(543);
        for(Long l : arrayList) {
            System.out.print(l + " ");
        }
        System.out.println();
        arrayList = tree.select(333);
        for(Long l : arrayList) {
            System.out.print(l + " ");
        }
        System.out.println();
        arrayList = tree.select(0);
        for(Long l : arrayList) {
            System.out.print(l + " ");
        }
        System.out.println();
        arrayList = tree.select(2000);
        for(Long l : arrayList) {
            System.out.print(l + " ");
        }
        System.out.println();
        
    }
    
    public static String byteArrayToHexString(byte[] array) {
        String result = "";
        for(int i = 0; i < 8; i++) {
            result += byteToHexString(array[i]);
        }
        result += " ";
        for(int i = 8; i < 12; i++) {
            result += byteToHexString(array[i]);
        }
        result += " ";
        for(int i = 12; i < 16; i++) {
            result += byteToHexString(array[i]);
        }
        for(int i = 16; i < array.length; i++) {
            if((i - 16) % (15*8) == 0) {
                result += "\n";
                result += Integer.toHexString(i).toUpperCase() + " ";
            }
            result += byteToHexString(array[i]);
            if(i%8 == 7) result += " ";
        }
        return result;
    }
    
    public static String byteToHexString(byte b) {
        String result = "";
        result += hexDigits[b>>>4 & 0x0F];
        result += hexDigits[b     & 0x0F];
        return result;
    }
    public static void testSearch() {
        keyValues.add(1);
        keyValues.add(3);
        keyValues.add(5);
        keyValues.add(7);
        keyValues.add(9);
        keyValues.add(11);
        keyValues.add(13);
        keyValues.add(15);
        keyValues.add(17);
        keyValues.add(19);
        System.out.println("value: -1, expected: -1, returns: " + m(-1)); 
        System.out.println("value: 0, expected: -1, returns: " + m(0));
        System.out.println("value: 1, expected: 0, returns: " + m(1));
        System.out.println("value: 2, expected: 0, returns: " + m(2));
        System.out.println("value: 3, expected: 1, returns: " + m(3));
        System.out.println("value: 4, expected: 1, returns: " + m(4));
        System.out.println("value: 5, expected: 2, returns: " + m(5));
        System.out.println("value: 6, expected: 2, returns: " + m(6));
        System.out.println("value: 7, expected: 3, returns: " + m(7));
        System.out.println("value: 8, expected: 3, returns: " + m(8));
        System.out.println("value: 9, expected: 4, returns: " + m(9));
        System.out.println("value: 10, expected: 4, returns: " + m(10));
        System.out.println("value: 11, expected: 5, returns: " + m(11));
        System.out.println("value: 12, expected: 5, returns: " + m(12));
        System.out.println("value: 13, expected: 6, returns: " + m(13));
        System.out.println("value: 14, expected: 6, returns: " + m(14));
        System.out.println("value: 15, expected: 7, returns: " + m(15));
        System.out.println("value: 16, expected: 7, returns: " + m(16));
        System.out.println("value: 17, expected: 8, returns: " + m(17));
        System.out.println("value: 18, expected: 8, returns: " + m(18));
        System.out.println("value: 19, expected: 9, returns: " + m(19));
        System.out.println("value: 20, expected: 9, returns: " + m(20));
        System.out.println("value: 21, expected: 9, returns: " + m(21));
        System.out.println("value: 22, expected: 9, returns: " + m(22));
    }
    
    public static int m(int key) {
        int start = 0;
        int end = keyValues.size() - 1;
        while (end - start > 0) {
            int middle = (start + end) / 2;
            long toCompare = keyValues.get(middle);
            if (toCompare > key) {
                end = middle;
            } else {
                start = middle + 1;
        }
    }
        long toCompare = keyValues.get(end);
        if (end + 1 >= keyValues.size() && toCompare <= key) return end;
        return end - 1;
    }
} 


    
