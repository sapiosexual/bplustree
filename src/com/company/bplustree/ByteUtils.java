package com.company.bplustree;


/**
 * Static class to convert Byte to Long and vice versa
 */
public class ByteUtils {
    private final static int LONG_SIZE = 8;
    private final static int INT_SIZE = 4;
    private final static int BITS_IN_BYTE = 8;

    /**
     * Convert long to byte[8]
     *
     * @param value long to convert
     * @return array of bytes
     */
    public static byte[] longToBytes(long value) {
        byte[] byteBuffer = new byte[LONG_SIZE];
        byteBuffer[0] = (byte) (value >>> BITS_IN_BYTE * 7);
        byteBuffer[1] = (byte) (value >>> BITS_IN_BYTE * 6);
        byteBuffer[2] = (byte) (value >>> BITS_IN_BYTE * 5);
        byteBuffer[3] = (byte) (value >>> BITS_IN_BYTE * 4);
        byteBuffer[4] = (byte) (value >>> BITS_IN_BYTE * 3);
        byteBuffer[5] = (byte) (value >>> BITS_IN_BYTE * 2);
        byteBuffer[6] = (byte) (value >>> BITS_IN_BYTE);
        byteBuffer[7] = (byte) (value);
        return byteBuffer;
    }

    /**
     * Convert int to byte[8]
     *
     * @param value long to convert
     * @return array of bytes
     */
    public static byte[] intToBytes(int value) {
        byte[] byteBuffer = new byte[INT_SIZE];
        byteBuffer[0] = (byte) (value >>> BITS_IN_BYTE * 3);
        byteBuffer[1] = (byte) (value >>> BITS_IN_BYTE * 2);
        byteBuffer[2] = (byte) (value >>> BITS_IN_BYTE);
        byteBuffer[3] = (byte) (value);
        return byteBuffer;
    }

    /**
     * Convert byte[8] to long
     *
     * @param bytes array of 8 bytes
     * @return long
     */
    public static long bytesToLong(byte[] bytes) {
        return bytesToLong(bytes, 0);
    }

    /**
     * Convert byte[] to long with given offset
     *
     * @param bytes  array of bytes
     * @param offset position to start
     * @return long
     */
    public static long bytesToLong(byte[] bytes, int offset) {
        if (checkLength(bytes, offset, 1, LONG_SIZE))
            throw new IllegalArgumentException("You try to parse more longs than array contains");
        return  ((long) (bytes[offset]    ) & 0xFF)     << BITS_IN_BYTE * 7 |
                ((long) (bytes[offset + 1]) & 0xFF) << BITS_IN_BYTE * 6 |
                ((long) (bytes[offset + 2]) & 0xFF) << BITS_IN_BYTE * 5 |
                ((long) (bytes[offset + 3]) & 0xFF) << BITS_IN_BYTE * 4 |
                ((long) (bytes[offset + 4]) & 0xFF) << BITS_IN_BYTE * 3 |
                ((long) (bytes[offset + 5]) & 0xFF) << BITS_IN_BYTE * 2 |
                ((long) (bytes[offset + 6]) & 0xFF) << BITS_IN_BYTE     |
                ((long) (bytes[offset + 7]) & 0xFF);
    }

    /**
     * Convert byte[4] to int
     *
     * @param bytes array of 4 bytes
     * @return int
     */
    public static int bytesToInt(byte[] bytes) {
        return bytesToInt(bytes, 0);
    }

    /**
     * Convert byte[] to int with given offset
     *
     * @param bytes  array of bytes
     * @param offset
     * @return long
     */
    public static int bytesToInt(byte[] bytes, int offset) {
        if (checkLength(bytes, offset, 1, INT_SIZE))
            throw new IllegalArgumentException("You try to parse more ints than array contains");
        return  bytes[offset]     & 0xFF << BITS_IN_BYTE * 3 |
                bytes[offset + 1] & 0xFF << BITS_IN_BYTE * 2 |
                bytes[offset + 2] & 0xFF << BITS_IN_BYTE     |
                bytes[offset + 3] & 0xFF;
    }

    /**
     * Convert array of longs to array of bytes
     *
     * @param values array of long
     * @return array of byte
     */
    public static byte[] arrayLongToBytes(long[] values) {
        byte[] result = new byte[values.length * LONG_SIZE];
        int pointer = 0;
        for (long value : values) {
            byte[] buffer = ByteUtils.longToBytes(value);
            System.arraycopy(buffer, 0, result, pointer, LONG_SIZE);
            pointer += LONG_SIZE;
        }
        return result;
    }

    /**
     * Convert array of longs to array of bytes
     *
     * @param values array of long
     * @return array of byte
     */
    public static byte[] arrayIntToBytes(int[] values) {
        byte[] result = new byte[values.length * INT_SIZE];
        int pointer = 0;
        for (int value : values) {
            byte[] buffer = ByteUtils.longToBytes(value);
            System.arraycopy(buffer, 0, result, pointer, INT_SIZE);
            pointer += INT_SIZE;
        }
        return result;
    }

    /**
     * Convert array of bytes to array of longs
     *
     * @param bytes the number of bytes in an array of multiple 8
     * @return array of long
     */
    public static long[] arrayBytesToArrayLong(byte[] bytes) {
        return arrayBytesToArrayLong(bytes, 0, bytes.length / LONG_SIZE);
    }

    /**
     * Convert array of bytes to array of longs
     *
     * @param bytes         the number of bytes in an array of multiple 8
     * @param offset        position to start
     * @param numberOfLongs how many longs should we parse
     * @return array of long
     */
    public static long[] arrayBytesToArrayLong(byte[] bytes, int offset, int numberOfLongs) {
        if (checkLength(bytes, offset, numberOfLongs, LONG_SIZE))
            throw new IllegalArgumentException("You try to parse more longs than array contains");
        long[] result = new long[numberOfLongs];
        for (int i = 0; i < numberOfLongs; i++) {
            result[i] = ByteUtils.bytesToLong(bytes, offset);
            offset += LONG_SIZE;
        }
        return result;
    }

    /**
     * Convert array of bytes to array of longs
     *
     * @param bytes the number of bytes in an array of multiple 8
     * @return array of long
     */
    public static int[] arrayBytesToArrayInt(byte[] bytes) {
        return arrayBytesToArrayInt(bytes, 0, bytes.length / INT_SIZE);
    }

    /**
     * Convert array of bytes to array of longs
     *
     * @param bytes        the number of bytes in an array of multiple 8
     * @param offset       position to start
     * @param numberOfInts how many longs should we parse
     * @return array of long
     */
    public static int[] arrayBytesToArrayInt(byte[] bytes, int offset, int numberOfInts) {
        if (checkLength(bytes, offset, numberOfInts, INT_SIZE))
            throw new IllegalArgumentException("You try to parse more ints than array contains");
        int[] result = new int[numberOfInts];
        for (int i = 0; i < numberOfInts; i++) {
            result[i] = ByteUtils.bytesToInt(bytes, offset);
            offset += INT_SIZE;
        }
        return result;
    }

    /**
     * @param bytes  source
     * @param offset position to start
     * @param number how many elements should we parse
     * @param size   size of element
     * @return
     */
    private static boolean checkLength(byte[] bytes, int offset, int number, int size) {
        return (bytes.length - offset) / size < number ?
                true :
                false;
    }
}