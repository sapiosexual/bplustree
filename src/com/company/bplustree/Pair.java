package com.company.bplustree;

/**
 * Created by exfool on 24.10.15.
 * Key-link pair
 */
public class Pair {
    public Long key;
    public Long link;

    public Pair(Long key, Long link) {
        this.key = key;
        this.link = link;
    }
}

