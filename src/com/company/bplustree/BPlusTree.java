package com.company.bplustree;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;


public class BPlusTree {
    public RandomAccessFile raf;
    protected long linkToRoot = 0; // link to start tree
    protected int height = 0; //Height of tree
    protected int k = 100; // count of pairs, fields compute 2*k + 1

    /**
     * Create B+ tree object
     *
     * @param file file with tree
     * @throws IOException if the file fail with any reason
     */
    public BPlusTree(File file) throws IOException {
        this.raf = new RandomAccessFile(file, "rw");
        pull();
    }

    public static void createNewTree(File file, int degree) throws IOException {
        file.createNewFile();
        byte[] bytes = new byte[8 + 4*2];
        System.arraycopy(ByteUtils.longToBytes(0), 0, bytes, 0, 8);
        System.arraycopy(ByteUtils.intToBytes(0), 0, bytes, 8, 4);
        System.arraycopy(ByteUtils.intToBytes(degree), 0, bytes, 12, 4);
        RandomAccessFile raf = new RandomAccessFile(file, "rw");
        raf.seek(0);
        raf.write(bytes);
    }

    /**
     * Insert pair of key->value into data set
     *
     * @param key   of pair
     * @param value of pair
     */
    public void insert(long key, long value) {
        try {
            Node root = getRoot();
            Pair pair = new Pair(key, value);
            root.insert(pair);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Delete all pairs with this key
     *
     * @param key for delete
     */
    public void delete(long key) {
        try {
            Node root = getRoot();
            root.delete(key);
        } catch (IOException e) {
            e.printStackTrace();
        }
        /*try {
            Node root = getRoot();
            boolean isDeleted = true;
            while (isDeleted) {
                isDeleted = root.delete(key);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }*/

    }

    /**
     * Select all values with this key
     *
     * @param key for select
     * @return set of values
     */
    public ArrayList<Long> select(long key) {
        try {
            Node root = getRoot();
            return root.select(key, key);
        } catch (IOException ex) {
        }
        return null;
    }
    
    /**
     * Select all values before this key
     *
     * @param key from -infinity to this key (included)
     * @return list of values
     */
    public ArrayList<Long> selectBefore(long key) {
        try {
            Node root = getRoot();
            return root.select(Long.MIN_VALUE, key);
        } catch (IOException ex) {
        }
        return null;
    }
    
    /**
     * Select all values after this key
     *
     * @param key from this key (included) to infinity
     * @return list of values
     */
    public ArrayList<Long> selectAfter(long key) {
        try {
            Node root = getRoot();
            return root.select(key, Long.MAX_VALUE);
        } catch (IOException ex) {
        }
        return null;
    }
    
    /**
     * Select all values in range
     *
     * @param from start of range (included)
     * @param to   end of range (included)
     * @return list of values
     */
    public ArrayList<Long> selectInRange(long from, long to) {
        try {
            Node root = getRoot();
            return root.select(from, to);
        } catch (IOException ex) {
        }
        return null;
    }

    /**
     * Select all values
     *
     * @return list of all values
     */
    public ArrayList<Long> selectAllValues() {
        try {
            Node root = getRoot();
            return root.select(Long.MIN_VALUE, Long.MAX_VALUE);
        } catch (IOException ex) {
        }
        return null;
    }

    /**
     * Select all keys
     *
     * @return set of all keys
     */
    public ArrayList<Long> selectAllKeys() {
        try {
            Node root = getRoot();
            return root.selectAllKeys();
        } catch (IOException ex) {
        }
        return null;
    }

    /**
     * Delete all data set
     */
    public void deleteAll() {
        //TODO deteleAll
    }

    /**
     * Traversal tree
     *
     * @return ArrayList of Array list sibling of ArrayList of ordered fields link-key-link
     */
    public ArrayList<ArrayList<ArrayList<Long>>> traversal() {
        //TODO traversal tree
        return null;
    }
    
    private Node getRoot() throws IOException {
        Node root;
        if(linkToRoot != 0) {
            root = new Node(linkToRoot, null, this);
        } else {
            root = new Node(null, this, 0);
        }
        return root;
    }

    /**
     * Pull tree meta information from file into RAM
     */
    private void pull() throws IOException {
        byte[] source = new byte[8 + 4*2];
        raf.seek(0);
        raf.read(source);
        linkToRoot = ByteUtils.bytesToLong(source);
        int[] values = ByteUtils.arrayBytesToArrayInt(source, 8, 2);
        height = values[0];
        k = values[1];
    }


    /**
     * Push tree meta information from RAM into file
     */
    protected void push() throws IOException {
        byte[] linkToNodeBytes = ByteUtils.longToBytes(linkToRoot);
        byte[] heightBytes = ByteUtils.intToBytes(height);
        byte[] raw = new byte[12];
        System.arraycopy(linkToNodeBytes, 0, raw, 0, 8);
        System.arraycopy(heightBytes, 0, raw, 8, 4);
        raf.seek(0);
        raf.write(raw);
    }
}
