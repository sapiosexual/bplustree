package com.company.bplustree;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Node {
    private Node parent;
    private BPlusTree tree;

    private long currentLink; //Link to this node
    protected long currentHeight; //Level of this node

    private ArrayList<Pair> keyValues = new ArrayList<>(); //Container pairs like key->values
    private long lastLink; // in Node: link to maximum child;  in Leaf: link to next leaf

    /**
     * Reads node from file
     *
     * @param currentLink offset to start node in file
     * @param parent      parent of this node
     * @param tree        current tree
     */
    public Node(long currentLink, Node parent, BPlusTree tree) {
        this.parent = parent;
        this.tree = tree;
        this.currentLink = currentLink;
        this.currentHeight = parent != null ? parent.currentHeight + 1 : 0;
        try {
            this.pull();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Create new node in memory with without values.
     * Current link will be contain end of file.
     *
     * @param parent   parent of new node
     * @param tree     current tree
     * @param lastLink link to next leaf or link to the most right child
     */
    public Node(Node parent, BPlusTree tree, long lastLink) throws IOException {
        this.parent = parent;
        this.tree = tree;
        this.currentLink = tree.raf.length();
        this.currentHeight = parent != null ? parent.currentHeight + 1 : 0;
        this.lastLink = lastLink;
    }

    /**
     * Create new node with pairs of key->value
     *
     * @param keyValues initial pairs for new node
     * @param parent    parent of new node
     * @param tree      current tree
     * @param lastLink link to next leaf or link to the most right child
     */
    public Node(List<Pair> keyValues, Node parent, BPlusTree tree, long lastLink) throws IOException{
        this(parent, tree, lastLink);
        this.keyValues.addAll(keyValues);
    }

    /**
     * Merge two node in one ( usage for delete )
     *
     * @param main   base for merge two nodes
     * @param adopt  append to main node and deleted after this
     * @param parent parent of new node
     * @param tree   current tree
     * @param lastLink link to next leaf or link to the most right child
     */
    public Node(Node main, Node adopt, Node parent, BPlusTree tree, long lastLink) throws IOException {
        this(parent, tree, lastLink);
        keyValues.addAll(main.keyValues);
        //TODO merge with lastLink, I'm totally forget.
        keyValues.addAll(adopt.keyValues);
    }

    /**
     * Pull node from file into RAM
     */
    private void pull() throws IOException{
        byte[] source = new byte[8 * 2 * tree.k + 8]; //size of long * longs per pair * number of pairs + additional link
        tree.raf.seek(currentLink);
        tree.raf.read(source);
        long[] values = ByteUtils.arrayBytesToArrayLong(source);
        lastLink = values[0];
        for (int i = 1; i < values.length; i+=2) {
            long key = values[i];//we store link before key
            long link = values[i + 1];
            if(!(key == 0 && link ==0)) { //if these values are zeros it means all next
                                          //values don't exist.
                keyValues.add(new Pair(key, link)); 
            } else {
                break;
            }
        }
    }


    /**
     * Push node from RAM into file
     */
    private void push() throws IOException {
        tree.raf.seek(currentLink);
        int pointer = 0;
        long[] values = new long[tree.k*2 + 1];
        values[pointer++] = lastLink;
        for(Pair p : keyValues) {
            values[pointer++] = p.key;
            values[pointer++] = p.link;
        }
        for(int i = pointer; i < tree.k * 2 + 1; i++) {
            values[i] = 0;
        }
        tree.raf.write(ByteUtils.arrayLongToBytes(values));
    }

    /**
     * Add pair of key->value to data set
     *
     * @param pair     of key->value
     * @param position position where we should insert pair
     */
    private void add(Pair pair, int position) { 
        if(position == - 1) position++; //uniques resolving. TODO add ununiques
        if(keyValues.isEmpty() || 
           position == keyValues.size() ||
           keyValues.get(position).key > pair.key) {
            keyValues.add(position, pair);
        } else if(keyValues.get(position).key < pair.key) {
            keyValues.add(position + 1, pair);
        } else {
            keyValues.set(position, pair);
        }
        if (keyValues.size() == tree.k) {
            expand();
        } else {
            try {
                push();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if(keyValues.size() == 1 && parent == null) { 
            try {
                //Congratulations, you are create the tree!
                increaseTreeHeight(currentLink);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * Used for expand. Add new key and new value between existing pair.
     *
     * @param key      key
     * @param nextLink link to right child
     */
    private void addWhenExpand(long key, long nextLink) {
        int position = findChild(key);
        Pair pair = new Pair(key, nextLink);
        add(pair, position + 1);
    }

    /**
     * Add pair of key->value to data set
     *
     * @param pair of key->value
     */
    public void insert(Pair pair) {
        int childPositionInArray = findChild(pair.key);
        if (isLeaf()) {
            add(pair, childPositionInArray);
        } else {
            long childAddress = childPositionInArray == -1 ?
                    lastLink :
                    keyValues.get(childPositionInArray).link;
            new Node(childAddress, this, tree).insert(pair);
            }
        }

    /**
     * Delete pair in leaf and reconstruct tree
     *
     * @param key for delete
     */
    public boolean delete(long key) {
        try {
            insert(new Pair(key, (long)0));
        } catch(Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Main method for recursive deleting and reconstructing tree
     * @param key key for deleting
     * @return some pair, if null didn't found it
     */
    public Pair deleteWithReturn(long key) throws Exception {
        int childPositionInArray = findChild(key);
        if (this.isLeaf()) {
            if (childPositionInArray == -1) { // -1 means that we haven't this element in array
                return null;
            } else {

                if (this.isRoot()) {
                    // if leaf is root
                    Pair result = keyValues.get(childPositionInArray);
                    keyValues.remove(childPositionInArray);
                    this.push();
                    return result;
                } else if (this.count() >= tree.k / 2) {
                    // then we delete, we have guarantee that this.count >= k/2, because if we do it recursively this prove parent
                    Pair result = keyValues.get(childPositionInArray);
                    keyValues.remove(childPositionInArray);
                    this.push();
                    return result;
                } else throw new Exception("Node delete: something wrong with delete leaf");
            }
        } else {
            long childAddress = childPositionInArray == -1 ?
                    lastLink :
                    keyValues.get(childPositionInArray).link;

            Node child = new Node(childAddress, this, tree);

            if (child.isLeaf()) {
                //if next node is child after delete check capacity
                if (child.count() > tree.k / 2) {
                    Pair result = child.deleteAndIfItFirstReturn(key);
                    child.push();
                    return result;
                } else {
                    // if child have only half count of elements.

                    long childAddressSibling =
                            childPositionInArray == -1 ?
                                    keyValues.get(0).link :
                                    childPositionInArray == 0 ?
                                            lastLink :
                                            keyValues.get(childPositionInArray - 1).link;

                    Node childSibling = new Node(childAddressSibling, this, tree); // notice we can not know parent of this element

                    if (childSibling.count() > tree.k / 2) {
                        Pair result = child.deleteWithReturn(key);
                        //if we can take one or more than element from sibling
                        Pair buff = childSibling.deleteMaxAndReturn(); // delete and return max element from leaf
                        childSibling.push();
                        child.insert(buff); // move element from sibling leaf to this leaf
                        child.push();
                        return result;
                    } else {
                        Pair result = child.deleteWithReturn(key);

                        //if we cann't take any element from subling need join they
                        if (childPositionInArray == 0) {
                            childSibling.give(child);
                            child.parentDeleteLeaf(key);
                            keyValues.remove(0);

                        } else {
                            child.give(childSibling);
                            childSibling.parentDeleteLeaf(key);

                            keyValues.remove(childPositionInArray == -1 ? 0 : childPositionInArray - 1);
                        }

                        child.push();
                        childSibling.push();
                        return result;
                    }

                }
            } else {
                Pair returned = child.deleteWithReturn(key);

                if (child.keyValues.size() == 0 && isRoot()) {
                    tree.linkToRoot = child.lastLink;
                    this.decreaseTreeHeight(tree.linkToRoot);
                    return null;
                }
                //after deleting check for zero statement, need up lastLink
                if (child.keyValues.size() <= tree.k / 2 && child.lastLink != 0) {
                    long childAddressSibling = childPositionInArray == -1 ?
                            keyValues.get(0).link :
                            childPositionInArray == 0 ?
                                    lastLink :
                                    keyValues.get(childPositionInArray - 1).link;
                    Node childSibling = new Node(childAddressSibling, this, tree); // notice we can not know parent of this element

                    if (childSibling.count() > tree.k / 2) {
                        //GET MAX ELEMENT FROM NEIGHBOR, REWRITE VALUE OF PARENT, PUSH THIS LINK TO CHILD
                        Pair buffer = childSibling.deleteMaxAndReturn(); //RETURN PAIR from left, valuer for root , link for right
                        Pair bufferThis = this.keyValues.get(childPositionInArray); // value from root

                        Pair bufferForChild = new Pair(bufferThis.key, child.lastLink);
                        child.add(bufferForChild, 0);
                        child.lastLink = buffer.link;

                        bufferThis.key = buffer.key;

                    } else {
                        //if we cann't take any element from sabling need join they
                        if (childPositionInArray == 0) {
                            childSibling.give(child);
                            child.parentDeleteLeaf(key);
                            keyValues.remove(0);
                        } else {
                            child.give(childSibling);
                            childSibling.parentDeleteLeaf(key);
                            keyValues.remove(childPositionInArray == -1 ? 0 : childPositionInArray - 1);
                        }
                    }

                    childSibling.push();

                }
                Pair currentPair = this.keyValues.get(childPositionInArray);
                if (currentPair.key == key) {
                    currentPair.key = returned.key;
                }

                child.push();
                return returned;
            }

        }
        //throw new Exception("Node deleteWithReturn: i can not imagine this situation");
    }

    /**
     * Delete some element, and if it's first, return them element and return them for dismemberment
     *
     * @param key for delete
     * @return null: if it was first, null: if it not first
     */
    public Pair deleteAndIfItFirstReturn(Long key) throws Exception {
        if (isLeaf() && keyValues.size() > 0) {
            int positionInArray = findChild(key);
            if (positionInArray == 0) {
                Pair buff = keyValues.get(0);
                keyValues.remove(0);
                return buff;
            } else {
                keyValues.remove(positionInArray);
                return null;
            }
        } else throw new Exception("Node deleteAndIfItFirstReturn: something wrong with it. isLeaf() && keyValues.size() > 0");
    }

    /**
     * Delete maximum element and return them for dismemberment
     *
     * @return pair with maximum key in this node
     */
    public Pair deleteMaxAndReturn() throws Exception {
        int lastIndex = this.keyValues.size() - 1;
        if (lastIndex >= 0) {
            Pair result = this.keyValues.get(lastIndex);
            this.keyValues.remove(lastIndex);
            return result;
        } else throw new Exception ("Node deleteMaxAndReturn: something wrong, haven't element");
    }

    /**
     * Delete void leaf from parent
     * in this parent check if keyValues.size >= tree.k/2 ;
     *
     * @param key of deleted child
     */
    public void parentDeleteLeaf(Long key) {
        //in this parent check if keyValues.size >= tree.k/2 ;
        this.keyValues.remove(findChild(key));
    }

    /**
     * Give all elements from this left to another sibling leaf and delete this key-link from parent
     *
     * @param main node for joining
     */
    public void give(Node main) {

        for (Pair pair : this.keyValues) {
            main.add( //TODO check add method : need only join 2 array
                    pair,
                    main.count()
            );
        }
        main.lastLink = this.lastLink;
        this.keyValues.clear();

    }

    /**
     * Get offset of this node
     *
     * @return offset to this node in long
     */
    private long getNodeLink() {
        return this.currentLink;
    }

    /**
     * Get values(links) in range in this node (note that keys can be duplicated)
     *
     * @param from start of range (included)
     * @param to   end of range (included)
     * @return Array list of pairs with this key
     */
    public ArrayList<Long> select(long from, long to) {
        int childPosition = findChild(from);
        if(isLeaf()) {
            ArrayList<Long> result = new ArrayList<>();
            if(childPosition == -1) childPosition++;
            int i = childPosition; //I know, it is bad, but it is neccessary
            for(; i < keyValues.size(); i++) {
                Pair p = keyValues.get(i);
                if(p.key >= from && p.key <= to) {
                    if(p.link != 0) result.add(p.link);
                } else {
                    break;
                }
            }
            if(i <= keyValues.size() && lastLink != 0) result.addAll(next().select(from, to));
            return result;
        } else {
            long childAddress = childPosition == -1 ?
                    lastLink :
                    keyValues.get(childPosition).link;
            return new Node(childAddress, this, tree).select(from, to);
        }
    }
    /**
     * this piece of shit should die in pain
     * @return array list of long all key
     */
    public ArrayList<Long> selectAllKeys() {
        if(isLeaf()) {
            ArrayList<Long> result = new ArrayList<>();
            keyValues.stream().forEach((keyValue) -> {
                result.add(keyValue.key);
            });
            if(lastLink != 0) result.addAll(next().selectAllKeys());
            return result;
        } else {
            return new Node(lastLink, this, tree).selectAllKeys();
        }
    }

    /**
     * Check node for state
     *
     * @return true: it's leaf; false: it's node
     */
    private boolean isLeaf() {
        return tree.height == 0 || //tree just created
               tree.height == 1 || //tree has only root
               currentHeight == tree.height - 1;
    }

    /**
     * Check node for state
     *
     * @return true: it's root; false: otherwise
     */
    private boolean isRoot() {
        return currentHeight == 0;
    }

    /**
     * Get count of exist pair(key->value)
     *
     * @return count of exist pair(key->value)
     */
    public int count() {
        return keyValues.size();
    }

    /**
     * If it's leaf then get next leaf (they are connected like single linked list)
     *
     * @return object of next leaf
     */
    private Node next() {
        if(isLeaf() && lastLink != 0) {
            return new ReadOnlyNode(lastLink, currentHeight, tree);
        }
        return null;
    }

    /**
     * Finds position of child in array
     *
     * @param key key for child
     * @return position in ArrayList
     */
    private int findChild(long key) {
        if(keyValues.isEmpty()) return -1;
        int start = 0;
        int end = keyValues.size() - 1;
        while (end - start > 0) {
            int middle = (start + end) / 2;
            long toCompare = keyValues.get(middle).key;
            if (toCompare > key) {
                end = middle;
            } else {
                start = middle + 1;
            }
        }
        long toCompare = keyValues.get(end).key;
        if (end + 1 >= keyValues.size() && toCompare <= key) return end;
        return end - 1;
    }

    /**
     * Divides current node to two parts
     */
    private void expand() {
        try {
            long keyToParent = keyValues.get(tree.k / 2).key;
            int newNodeStart = isLeaf() ?
                    tree.k / 2:
                    tree.k / 2 + 1;
            ArrayList<Pair> oldKeyValues = keyValues;
            keyValues = new ArrayList<>();
            keyValues.addAll(oldKeyValues.subList(0, tree.k / 2));
            long siblingLastLink = isLeaf() ?
                    lastLink:
                    oldKeyValues.get(tree.k / 2).link;
            Node rightSibling = new Node(
                    oldKeyValues.subList(newNodeStart, oldKeyValues.size()),
                    parent,
                    tree,
                    siblingLastLink
            );
            lastLink = isLeaf() ?
                    rightSibling.getNodeLink() :
                    lastLink;
            this.push();
            rightSibling.push();
            if (isRoot()) {
                Node root = new Node(null, tree, currentLink);
                root.add(new Pair(keyToParent, rightSibling.getNodeLink()), 0); //TODO refactor constructor
            } else {
                parent.addWhenExpand(keyToParent, rightSibling.getNodeLink());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void increaseTreeHeight(long currentLink) throws IOException {
        tree.height++;
        tree.linkToRoot = currentLink;
        tree.push();
    }

    private void decreaseTreeHeight(long currentLink) throws IOException {
        tree.height--;
        tree.linkToRoot = currentLink;
        tree.push();
    }
}
