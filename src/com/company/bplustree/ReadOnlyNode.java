package com.company.bplustree;

/**
 *
 * @author Admin
 */
public class ReadOnlyNode extends Node {

    protected ReadOnlyNode(long currentLink, long currentHeight, BPlusTree tree) {
        super(currentLink, null, tree);
        this.currentHeight = currentHeight;
    }

    @Override
    public boolean delete(long key) {
        throw new UnsupportedOperationException("You cannot delete info from this node");
    }

    @Override
    public void insert(Pair pair) {
        throw new UnsupportedOperationException("You cannot insert info into this node");
    }
    
    
    
}
